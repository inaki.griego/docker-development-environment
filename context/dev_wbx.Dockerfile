# Image with the environment to crosscompile for RPI compute module.
FROM registry.gitlab.com/wallbox/embedded/embedded-cross-compiling/embedded-cross-compiling:latest as rpi-toolchain

# Image with the environment to crosscompile for ATLAS compute module.
FROM registry.gitlab.com/wallbox/embedded/embedded-supernova/embedded-supernova:latest as atlas-toolchain

# Image with the environment to build for x86 and run tests.
FROM registry.gitlab.com/wallbox/embedded/embedded-testing/embedded-testing:latest

# From the RPI image, add the toolchain and qt5 dependencies to the final image.
COPY --from=rpi-toolchain /opt/poky /opt/poky
COPY --from=rpi-toolchain /usr/bin/qt5 /usr/bin/qt5

# From the ATLAS image, add the toolchain.
COPY --from=atlas-toolchain /opt/wb-wayland /opt/wb-wayland

# Create a user 'user' with password 'user'. Change user 'root' password to 'root'. Give user 'user' root priviledges.
RUN useradd -rm -d /home/user -s /bin/bash -g root -G sudo user && echo "user:user" | chpasswd \
    && echo "root:root" | chpasswd && echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# Restore source.list and add needed packages to customize the image. Remote dev, ssh, etc. !! Add your packages here !!
RUN echo 'deb http://archive.ubuntu.com/ubuntu/ focal main restricted universe multiverse' >> /etc/apt/sources.list \
    && apt update && apt install -y rsync nano openssh-server gdb graphviz && apt clean

# Setup the entrypoint. Entrypoint will start the necesary services. Add any needed startup step there.
COPY entrypoint.sh /root/entrypoint.sh
CMD ["/root/entrypoint.sh"]
